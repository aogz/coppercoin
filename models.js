var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Currency = new Schema({
	title: String,
	symbol: String,
})

var Wallet = new Schema({
    title: String,
    balance: {type: Number, default: 0.0},
    currency: {type: Schema.Types.ObjectId, ref: 'Currency'}
});

var Payment = new Schema({
	title:String,
	description: String,
	sum: Number,
	wallet: {type: Schema.Types.ObjectId, ref: 'Wallet'},
	currency: {type: Schema.Types.ObjectId, ref: 'Currency'},
	type: Boolean,
	date: Date,
	accepted: Boolean
})

module.exports.Wallet = mongoose.model('Wallet', Wallet);
module.exports.Currency = mongoose.model('Currency', Currency);
module.exports.Payment = mongoose.model('Payment', Payment);
