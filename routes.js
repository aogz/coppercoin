var models = require('./models');
var path = require('path');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = function(app) {
	// app.param('name', function(req, res, next, name) {
	//     // check if the user with that name exists
	//     // do some validations
	//     // add -dude to the name
	//     var modified = name + '-dude';

	//     // save name to the request
	//     req.name = modified;

	//     next();
	// });

	app.route('/api/payments/:id?')
		.get(function(req, res) {
			var id = req.params.id;
			if (id) {
				models.Payment.find({id: id}).populate('wallet').populate('currency').exec(function(err, data){
					if (err) res.send(err);
					res.json(data);
				});
			} else {
				models.Payment.find().populate('wallet').populate('currency').exec(function(err, data){
					if (err) res.send(err);
					res.json(data);
				});
			}
		})
		.post(function(req, res) {
			type = req.body.type ? req.body.type : false;
			accepted = req.body.accepted ? req.body.accepted : false;

			models.Payment.create({
				title: req.body.title,
				sum: req.body.sum,
				description: req.body.description,
				wallet: req.body.wallet,
				currency: req.body.currency,
				type: type,
				date: req.body.date,
				accepted: accepted
			}, function(err, payment) {
				console.log(payment);
				if (err) res.send(err);

				models.Payment.find().populate('wallet').populate('currency').exec(function(err, data){
					if (err) res.send(err);
					res.json(data);
				});
			})
		})
		.put(function(req, res) {
			var id = req.params.id;

			models.Payment.findOneAndUpdate({_id: id}, req.body, function(err, payment) {
				if (err) res.send(err);

				models.Payment.find().populate('wallet').populate('currency').exec(function(err, data){
					res.json(data);
				});
			})
		})
		.delete(function(req, res) {
			var id = req.params.id;

			models.Payment.find({_id: id}).remove(function(err, payment) {
				if (err) res.send(err);

				models.Payment.find().populate('wallet').populate('currency').exec(function(err, data){
					res.json(data);
				});
			})
		})

	// app.route('/api/currency/:id?')
	// 	.get(function(req, res) {

	// 	})
	// 	.post(function(req, res) {

	// 	})
	// 	.put(function(req, res) {

	// 	})
	// 	.delete(function(req, res) {

	// 	})

	// app.route('/api/wallet/:id?')
	// 	.get(function(req, res) {

	// 	})
	// 	.post(function(req, res) {

	// 	})
	// 	.put(function(req, res) {

	// 	})
	// 	.delete(function(req, res) {

	// 	})

	app.get('/api/get-user-data', function(req, res) {
		response = {}

		models.Payment.find().populate('wallet').populate('currency').exec(function(err, data){
			response.payments = data;

			models.Currency.find().exec(function(err, data){
				response.currencies = data;

				models.Wallet.find().populate('currency').exec(function(err, data){
					response.wallets = data;
					res.json(response)

				});

			});
		});
	});

	// application 
	app.get('/', function(req, res) {
		res.sendFile(path.join(__dirname, '/templates', 'base.html'))
	});
};
