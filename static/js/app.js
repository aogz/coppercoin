var app = angular.module('CopperCoinApp', ['ngRoute'])

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.

    when('/', {
        templateUrl: '/static/ng/templates/index.html',
        // controller: 'PaymentsCtrl'
    }).

    when('/settings', {
        templateUrl: '/static/ng/templates/settings.html',
        controller: 'SettingsCtrl'
    }).

    when('/sign-in', {
        templateUrl: '/static/ng/templates/sign-in.html',
        controller: 'SettingsCtrl'
    }).

    when('/sign-up', {
        templateUrl: '/static/ng/templates/sign-up.html',
        controller: 'SettingsCtrl'
    }).

    otherwise({
        redirectTo: '/'
    })
}]);
