app.controller('PaymentsCtrl', function($scope, $http){
    $scope.payments = [];
    $scope.payment = {};
    $scope.wallet_filter = "";
    $scope.accepted_filter = "";

    $scope.init = function() {
        $http.get('/api/get-user-data', {}, {}).then(
            function(response){
                if (response.data.errors) {
                    Materialize.toast(response.data.errors.message, 4000);
                    return false
                }

                $scope.payments = response.data.payments;
                $scope.wallets = response.data.wallets;
                $scope.currencies = response.data.currencies;

            }, 
            function(response){
                Materialize.toast('Internal server error.', 4000);
            }
            );
    }

    $scope.add_payment = function(payment){
        if ((!$scope.payment.title) || (!$scope.payment.sum) || (!$scope.payment.currency) || (!$scope.payment.wallet)){
            Materialize.toast('Please complete all fields', 4000);
            return false;
        }

        $http.post('/api/payments', payment).then(
            function(response){
                if (response.data.errors) {
                    Materialize.toast(response.data.errors.message, 4000);
                    return false
                }

                Materialize.toast('Saved', 4000);
                $scope.payments = response.data;
                console.log($scope.payments)
            }, 
            function(response){
                Materialize.toast('Internal server error.', 4000);
            }
        );
}

$scope.remove_payment = function(payment) {
    $http.delete('/api/payments/' + payment._id, payment).then(
        function(response){
            if (response.data.errors) {
                Materialize.toast(response.data.errors.message, 4000);
                return false
            }

            console.log(response)
            Materialize.toast('Deleted', 4000);
            $scope.payments = response.data;
        }, 
        function(response){
            Materialize.toast('Internal server error.', 4000);
        }
        );
}

$scope.accept_payment = function(payment) {
    var payment = payment;
    payment.accepted = true;
    $http.put('/api/payments/' + payment._id, payment).then(
        function(response){
            if (response.data.errors) {
                Materialize.toast(response.data.errors.message, 4000);
                return false
            }

            Materialize.toast('Accepted', 4000);
            $scope.payments = response.data;
        }, function(response){
            Materialize.toast('Internal server error.', 4000);
        }
    );
}

$scope.init();

});